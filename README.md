PASOS PARA INSTALAR Y EJECUTAR LA APLICACIÓN MÓVIL FLUTTER:

•	Instalar Android Studio Versión Android Studio Bumblebee | 2021.1.1 Patch 3 Build #AI-211.7628.21.2111.8309675, built on March 16, 2022

•	Instalar Flutter en el Sistema operativo https://docs.flutter.dev/get-started/install?gclid=Cj0KCQjwmuiTBhDoARIsAPiv6L9scksWRixvBSOFOWg-ChdRZ2MYYR37gQ8NHy0DXNerKbmKVToB6vkaAhuvEALw_wcB&gclsrc=aw.ds 

•	Instalar el Plugin de Flutter en Android Studio

•	Clonar repositorio

•	Abrir Android Studio y buscar la carpeta “comidapp-master” para abrir el proyecto

•	Cuado el proyecto haya abierto por completo, buscar el archivo “DependencyInjection” y modificar el elemento “baseUrl” por la URL del servidor donde haya levantado el backend.

•	Ejecutar la aplicación
