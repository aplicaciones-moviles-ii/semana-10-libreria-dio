import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/location_service.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class SetLocationOnMapScreen extends StatefulWidget {
  const SetLocationOnMapScreen({Key? key}) : super(key: key);

  @override
  State<SetLocationOnMapScreen> createState() => _SetLocationOnMapScreenState();
}

class _SetLocationOnMapScreenState extends State<SetLocationOnMapScreen> {
  final Location location = Location();
  String? latitude="0.0", longitude="0.0", country, adminArea;

  late GoogleMapController googleMapController;
  CameraPosition initialCameraPosition =
  const CameraPosition(target: LatLng(41.40677280107817, 2.19458159747854), zoom: 14);
  Set<Marker> markers = {};

  @override
  void initState() {
    super.initState();
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: PaddingScreen),
        child: Column(
          children: [
            Expanded(
              child: GoogleMap(
                initialCameraPosition: initialCameraPosition,
                markers: markers,
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                zoomControlsEnabled: true,
                mapType: MapType.normal,
                onMapCreated: (GoogleMapController controller) {
                  googleMapController = controller;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(PaddingBetweenWidgets),
              child: Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text("Longitude:"),
                      Text("Latitude:"),
                      Text("Area:"),
                      Text("Country:"),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(PaddingScreen),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(longitude ?? "No longitude data"),
                        Text(latitude ?? "No latitude data"),
                        Text(adminArea ?? "No area data"),
                        Text(country ?? "No country data"),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            ElevatedButton(
                onPressed: () {
                  Navigator.pop(context, [longitude, latitude]);
                },
                style: LocalButtonStyle,
                child: const Text("SET",style: RegularMediumButtonTextStyle)),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.small(
        onPressed: () async {
          getLocation();
        },
        backgroundColor: localAccentColor,
        child: const Icon(Icons.gps_fixed),
      ),
    );
  }

  void getLocation() async {
    final service = LocationService();
    final locationData = await service.getLocation();

    if(locationData != null){

      final placemark = await service.getPlaceMark(locationData: locationData);

      setState(() {
        latitude = locationData.latitude!.toStringAsFixed(6);
        longitude = locationData.longitude!.toStringAsFixed(6);
        country = placemark?.country;
        adminArea = placemark?.administrativeArea;

        googleMapController.animateCamera(
            CameraUpdate.newCameraPosition(
                CameraPosition(
                    target: LatLng(double.parse(latitude!),double.parse(longitude!)),
                    zoom: 14)));
        markers.clear();
        markers.add(
            Marker(
                markerId: const MarkerId("Current position"),
                position: LatLng(double.parse(latitude!),double.parse(longitude!)),
            ));

      });
    }
  }

}
