import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/standard_response.dart';
import 'package:comidapp/screens/local_screens/create_events_screen.dart';
import 'package:comidapp/screens/local_screens/local_main_screen.dart';
import 'package:comidapp/screens/local_screens/set_location_on_map_screen.dart';
import 'package:comidapp/screens/local_screens/upload_food_pictures.dart';
import 'package:comidapp/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:comidapp/models/food.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';

class PublishedFoodDetail extends StatefulWidget {
  final Food food;
  const PublishedFoodDetail({Key? key, required this.food}) : super(key: key);

  @override
  State<PublishedFoodDetail> createState() => _PublishedFoodDetailState();
}

class _PublishedFoodDetailState extends State<PublishedFoodDetail> {
  late String title='', description='', city='';
  late int pvp=0, maxTourist=0;
  String? longitude, latitude;
  String? urlImage;

  final FoodAPI localFoodAPI = GetIt.instance<FoodAPI>();

  Future<void> _submit() async {
    ProgressDialog.show(context);
    final HttpResponse<StandardResponse> httpResponse = await localFoodAPI.updateFood(
        widget.food.id,
        title,
        description,
        pvp.toString(),
        longitude!,
        latitude!,
        city,
        maxTourist.toString(),
        urlImage!
    );
    ProgressDialog.dissmiss(context);
    if(httpResponse.data != null){
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LocalMainScreen(indexOutside: 0,)),
            (Route<dynamic> route) => false,
      );
    }else
    {
      String message = httpResponse.error!.message;
      if(httpResponse.error!.statusCode == 500){
        message = "No network access";
      }else if (httpResponse.error!.statusCode == 422){
        message = "Error de validación de datos";
      }
      Dialogs.alert(context, "ERROR", message);
    }
  }

  @override
  Widget build(BuildContext context) {
    //Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child:
            Padding(
              padding: const EdgeInsets.all(PaddingScreen),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:[
                    TextField(
                      controller: TextEditingController(text: widget.food.name),
                      onChanged: (value) {
                        title = value;
                      },
                      decoration: localTextFieldDecoration.copyWith(hintText: 'Title'),
                    ),
                    const SizedBox(height: PaddingBetweenWidgets,),
                    TextField(
                      controller: TextEditingController(text: widget.food.city),
                      onChanged: (value) {
                        city = value;
                      },
                      decoration: localTextFieldDecoration.copyWith(hintText: 'City'),
                    ),
                    const SizedBox(height: PaddingBetweenWidgets,),
                    TextField(
                      controller: TextEditingController(text: widget.food.description),
                      keyboardType: TextInputType.multiline,
                      maxLines: 4,
                      onChanged: (value) {
                        description = value;
                      },
                      decoration: localTextFieldDecoration.copyWith(hintText: 'Description'),
                    ),
                    const SizedBox(height: PaddingBetweenWidgets,),
                    TextField(
                      controller: TextEditingController(text: widget.food.maxTourist.toString()),
                      onChanged: (value) {
                        maxTourist =  int.parse(value);
                      },
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      decoration: localTextFieldDecoration.copyWith(hintText: 'Max tourist per event'),
                    ),
                    const SizedBox(height: PaddingBetweenWidgets,),
                    TextField(
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      controller: TextEditingController(text: widget.food.pvp.substring(0,2)),
                      onChanged: (value) {
                        pvp =  int.parse(value);
                      },
                      decoration: localTextFieldDecoration.copyWith(hintText: 'Price'),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                    return const SetLocationOnMapScreen();
                                  }));
                            },
                            child:
                            const Text('Edit location on map', style: RegularMediumTextStyle,)),
                        const Icon(Icons.arrow_right),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:PaddingBetweenWidgets),
                      child: Row(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Longitude:"),
                              Text("Latitude:"),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(widget.food.longitude),
                                Text(widget.food.latitude),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                    return CreateEventsScreen(foodId: widget.food.id,);
                                  }));
                            },
                            child:
                            const Text('Edit events', style: RegularMediumTextStyle,)),
                        const Icon(Icons.arrow_right),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                    return const UploadFoodPictures();
                                  }));
                            },
                            child:
                            const Text('Edit main picture', style: RegularMediumTextStyle,)),
                        const Icon(Icons.arrow_right),
                      ],
                    ),

                    Padding(
                      padding: const EdgeInsets.only(left:PaddingBetweenWidgets),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("URL Image:"),
                          Text(widget.food.image, overflow: TextOverflow.ellipsis,),
                        ],
                      ),
                    ),
                    const SizedBox(height: 12,),
                    TextButton(
                        onPressed: () {
                          _submit();
                        },
                        child: const Text(
                          'Done',style: RegularMediumButtonTextStyle,
                        ),
                        style: LocalButtonStyle
                    ),
                  ]
              ),
            ),

        ),
    );
  }
}
