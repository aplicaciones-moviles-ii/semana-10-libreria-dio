import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/components/local_user_components/local_event_list_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/helpers/singleton.dart';
import 'package:comidapp/models/create_event_response.dart';
import 'package:comidapp/models/event.dart';
import 'package:comidapp/screens/local_screens/local_main_screen.dart';
import 'package:comidapp/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class CreateEventsScreen extends StatefulWidget {
  final int foodId;

  const CreateEventsScreen({Key? key, required this.foodId}) : super(key: key);

  @override
  State<CreateEventsScreen> createState() => _CreateEventsScreenState();
}

class _CreateEventsScreenState extends State<CreateEventsScreen> {
  late DateTime date;
  late TimeOfDay time;
  late String dateSelectedInText = "Select date";
  late String timeSelectedInText = "Select time";
  final EventAPI eventAPI = GetIt.instance<EventAPI>();

  int selectedEventIndex = -1;
  late Event eventSelected;

  void selectEventItem(int index, Event event) {
    selectedEventIndex = index;
    eventSelected = event;
    setState(() {});
  }

  void setDateToText() {
    date == null
        ? dateSelectedInText = "Select date"
        : dateSelectedInText = '${date.year}-${date.month.toString().padLeft(2, '0')}-${date.day}';
  }

  // void setTimeToText() {
  //   if (time == null) {
  //     timeSelectedInText = "Select time";
  //   } else {
  //     final hours = time.hour.toString().padLeft(2, '0');
  //     final minutes = time.minute.toString().padLeft(2, '0');
  //     timeSelectedInText = '$hours:$minutes';
  //   }
  // }

  Future pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDate = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime.now(),
      lastDate: DateTime(DateTime.now().year + 5),
    );
    if (newDate == null) return;
    setState(() {
      date = newDate;
      dateSelectedInText = Singleton().setDateToText(date);
    });
  }

  Future pickTime(BuildContext context) async {
    final initialTime = TimeOfDay.now();
    final newTime =
        await showTimePicker(context: context, initialTime: initialTime);
    if (newTime == null) return;
    setState(() {
      time = newTime;
      timeSelectedInText = Singleton().setTimeToText(time);
      //setTimeToText();
    });
  }

  Future<void> _submit() async {
    ProgressDialog.show(context);
    final HttpResponse<CreateEventResponse> httpResponse = await eventAPI
        .addEventToFood(widget.foodId, dateSelectedInText, timeSelectedInText);
    ProgressDialog.dissmiss(context);
    if (httpResponse.data != null) {
    } else {
      String message = httpResponse.error!.message;
      if (httpResponse.error!.statusCode == 500) {
        message = "No network access";
      } else if (httpResponse.error!.statusCode == 422) {
        message = "Error de validación de datos";
      }
      Dialogs.alert(context, "ERROR", message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 24.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                  onPressed: () {
                    pickDate(context);
                  },
                  child: const Text(
                    'Set Date',
                    style: RegularMediumTextStyle,
                  )),
              const Icon(Icons.arrow_right),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                  onPressed: () {
                    pickTime(context);
                  },
                  child: const Text(
                    'Set Hour',
                    style: RegularMediumTextStyle,
                  )),
              const Icon(Icons.arrow_right),
            ],
          ),
          Text(
            dateSelectedInText,
            style: BoldLargeTextStyle,
          ),
          Text(
            timeSelectedInText,
            style: BoldLargeTextStyle,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: TextButton(
                onPressed: () {
                  _submit();
                  setState(() {
                  });
                },
                child: const Text(
                  'Add event',
                  style: RegularMediumButtonTextStyle,
                ),
                style: LocalButtonStyle),
          ),
           SizedBox(
             width: MediaQuery.of(context).size.width,
             height: MediaQuery.of(context).size.height*0.35,
             child: LocalEventListComponent(
              idFood: widget.foodId,
              onTap: selectEventItem,
              selectedIndex: selectedEventIndex,
          ),
           ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: TextButton(
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LocalMainScreen(indexOutside: 0)),
                    (Route<dynamic> route) => false,
                  );
                },
                child: const Text(
                  'Done',
                  style: RegularMediumButtonTextStyle,
                ),
                style: LocalButtonStyle),
          ),
        ],
      ),
    );
  }
}
