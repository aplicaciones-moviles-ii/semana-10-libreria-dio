import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/screens/general_screens/profile_screen.dart';
import 'package:comidapp/screens/general_screens/sign_in_screen.dart';
import 'package:flutter/material.dart';
import 'local_main_screen.dart';

class LocalConfigurationScreen extends StatelessWidget {
  const LocalConfigurationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(Icons.verified_user),
              TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                          return const ProfileScreen();
                        }));
                  },
                  child:
                  const Text('My profile', style: RegularMediumTextStyle,)),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(Icons.food_bank),
              TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                          return LocalMainScreen(indexOutside: 0,);
                        }));
                  },
                  child:
                  const Text('User guide', style: RegularMediumTextStyle,)),
            ],
          )
          ,
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(Icons.door_back_door),
              TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                          return const SignInScreen();
                        }));
                  },
                  child:
                  const Text('Log out', style: RegularMediumTextStyle,)),
            ],
          )
        ],
      ),
    );
  }
}
