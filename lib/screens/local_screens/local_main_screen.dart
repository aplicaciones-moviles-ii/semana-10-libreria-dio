import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/screens/local_screens/local_configuration_screen.dart';
import 'package:comidapp/screens/local_screens/published_foods_screen.dart';
import 'package:flutter/material.dart';

class LocalMainScreen extends StatefulWidget {
  int indexOutside = 0;
  LocalMainScreen({Key? key, required this.indexOutside}) : super(key: key);

  @override
  State<LocalMainScreen> createState() => _LocalMainScreenState();
}

class _LocalMainScreenState extends State<LocalMainScreen> {

  int _selectedIndex = 0;

  final screens = [
    PublishedFoodsScreen(),
    LocalConfigurationScreen()
  ];

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.indexOutside;
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.food_bank),
            label: 'Published Foods',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Configuration',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        backgroundColor: localAccentColor,
        onTap: _onItemTapped,
      ),
    );
  }
}
