import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/components/general_components/food_item_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/foods_response.dart';
import 'package:comidapp/screens/local_screens/publish_new_food_screen.dart';
import 'package:comidapp/screens/local_screens/published_food_detail.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:comidapp/models/food.dart';

class PublishedFoodsScreen extends StatefulWidget {
  const PublishedFoodsScreen({Key? key}) : super(key: key);

  @override
  State<PublishedFoodsScreen> createState() => _PublishedFoodsScreenState();
}

class _PublishedFoodsScreenState extends State<PublishedFoodsScreen> {
  final FoodAPI localFoodAPI = GetIt.instance<FoodAPI>();

  Future<List<Food>> loadFoods() async {
    HttpResponse<FoodsResponse> response = await localFoodAPI.getFoodByUser();
    return response.data!.foods;
  }

  void showPublishedFoodDetail(Food food) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return PublishedFoodDetail(
        food: food,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Food>>(
        future: loadFoods(),
        builder: (context, snapshot) {
          Widget child = const Text("Connecting");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          if (snapshot.data!.isNotEmpty) {
            child = ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  List<Food>? lista = snapshot.data;
                  return Container(
                    margin: const EdgeInsets.all(12),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 12, vertical: 12),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      boxShadow: [
                        BoxShadow(
                            color: localPrimaryColor, offset: Offset(4, 4))
                      ],
                    ),
                    child: FoodItemComponent(
                      food: lista![index],
                      onTap: showPublishedFoodDetail,
                    ),
                  );
                });
          } else {
            child = const Center(child: Text("NO FOODS PUBLISHED", style: RegularLargeTextStyle,));
          }
          return Container(
            child: child,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showPublishNewFoodScreen();
        },
        tooltip: 'New Food',
        child: const Icon(Icons.add),
      ), // This
    );
  }

  void showPublishNewFoodScreen() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const PublishNewFoodScreen();
    }));
  }
}
