import 'package:comidapp/api/authentication_api.dart';
import 'package:comidapp/components/general_components/banner_image_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/authentication_response.dart';
import 'package:comidapp/screens/local_screens/local_main_screen.dart';
import 'package:comidapp/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _formKey = GlobalKey<FormState>();
  final AuthenticationAPI authenticationAPI = GetIt.instance<AuthenticationAPI>();
  final AuthenticationClient authenticationClient = GetIt.instance<AuthenticationClient>();

  String email='', password='';

  Future<void> _submit() async {
    ProgressDialog.show(context);

    final HttpResponse<AuthenticationResponse> httpResponse = await authenticationAPI.login(email, password);
    ProgressDialog.dissmiss(context);
    if(httpResponse.data != null){
      await authenticationClient.saveSession(httpResponse.data!);

      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LocalMainScreen(indexOutside: 0)),
            (Route<dynamic> route) => false,
      );
    }else
      {
        String message = httpResponse.error!.message;
        if(httpResponse.error!.statusCode == -1){
          message = "No network access";
        }else if (httpResponse.error!.statusCode == 422){
          message = "Email or password incorrect!";
        }
        Dialogs.alert(context, "ERROR", message);
      }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children:  [
              const BannerImagesComponent(),
              Padding(
                padding: const EdgeInsets.all(PaddingScreen),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[

                      TextFormField(
                        validator: (value) {
                          if (value!.isEmpty ||
                              !RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$')
                                  .hasMatch(value)) {
                            return "Email incorrect format";
                          } else {
                            return null;
                          }
                        },
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.name,
                        onChanged: (value) {
                          email = value;
                        },
                        decoration: textFieldDecoration.copyWith(hintText: 'Enter your email'),
                      ),
                      const SizedBox(height: PaddingBetweenWidgets,),
                      TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password cannot be empty';
                          } else {
                            return null;
                          }
                        },
                        textAlign: TextAlign.center,
                        obscureText: true,
                        onChanged: (value) {
                          password = value;
                        },
                        decoration: textFieldDecoration.copyWith(hintText: 'Enter your password'),
                      ),
                      const SizedBox(height: PaddingBetweenWidgets,),
                      TextButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate() == true) {
                              _submit();
                            }
                          },
                          child: const Text(
                            'Log In',style: RegularMediumButtonTextStyle,
                          ),
                          style: TouristButtonStyle
                      ),
                    ],
                  ),
                ),
              ),
            ]),
      ),
    );
  }
}
