import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/components/general_components/banner_image_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/screens/general_screens/sign_in_screen.dart';
import 'package:comidapp/screens/local_screens/local_main_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {

  final AuthenticationClient authenticationClient = GetIt.instance<AuthenticationClient>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_){
      checkLogin();
    });
  }

  Future<void> checkLogin() async {
    final token = await authenticationClient.accessToken;
    if(token == null){
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => SignInScreen()),
            (Route<dynamic> route) => false,
      );
    }else{
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LocalMainScreen(indexOutside: 0)),
            (Route<dynamic> route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children:  [
          const BannerImagesComponent(),
          Padding(
            padding: const EdgeInsets.all(PaddingScreen),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children:  const [
                Text("Bienvenido a", style: RegularMediumTextStyle),
                Text("ComidApp", style: BoldLargeTextStyle),
                SizedBox(height: PaddingBetweenWidgets,),
              ],
            ),
          ),
        ],
      ),
    ) ;
  }
}
