import 'package:flutter/material.dart';

const SmallTextSize = 10.0;
const MediumTextSize = 15.0;
const LargeTextSize = 20.0;

const PaddingBetweenWidgets = 6.0;
const PaddingScreen = 24.0;

const TextColor = Colors.black;

const primaryColor = Colors.teal;
const primaryAccentColor = Colors.tealAccent;

const localPrimaryColor = Colors.lightBlueAccent;
const localAccentColor = Colors.blue;

const String DefaultFontName = 'Montserrat';

// BOLD TEXT STYLES
const BoldLargeTextStyle =
TextStyle(color: TextColor, fontSize: LargeTextSize, fontWeight: FontWeight.w600, fontFamily: DefaultFontName);

const BoldMediumTextStyle =
TextStyle(color: TextColor, fontSize: MediumTextSize, fontWeight: FontWeight.w600, fontFamily: DefaultFontName);

const BoldSmallTextStyle =
TextStyle(color: TextColor, fontSize: SmallTextSize, fontWeight: FontWeight.w600, fontFamily: DefaultFontName);
// BOLD TEXT STYLES


// NORMAL TEXT STYLES
const RegularLargeTextStyle =
TextStyle(color: TextColor, fontSize: LargeTextSize, fontWeight: FontWeight.w300, fontFamily: DefaultFontName);

const RegularMediumTextStyle =
TextStyle(color: TextColor, fontSize: MediumTextSize, fontWeight: FontWeight.w300, fontFamily: DefaultFontName);

const RegularSmallTextStyle =
TextStyle(color: TextColor, fontSize: SmallTextSize, fontWeight: FontWeight.w300, fontFamily: DefaultFontName);
// NORMAL TEXT STYLES

//***************************************TOURIST BUTTON STYLES
const buttonTextColor = Colors.white;
const RegularMediumButtonTextStyle =
TextStyle(color: buttonTextColor, fontSize: MediumTextSize, fontWeight: FontWeight.w600, fontFamily: DefaultFontName);

const BoldSmallButtonTextStyle =
TextStyle(color: primaryColor, fontSize: SmallTextSize, fontWeight: FontWeight.w600, fontFamily: DefaultFontName);

const BoldMediumButtonTextStyle =
TextStyle(color: primaryColor, fontSize: MediumTextSize, fontWeight: FontWeight.w600, fontFamily: DefaultFontName);

ButtonStyle TouristButtonStyle =
ButtonStyle(
minimumSize: MaterialStateProperty.all<Size>(const Size(200.0, 50.0)),
backgroundColor: MaterialStateProperty.all<Color>(primaryColor));
//TOURIST BUTTON STYLES

// LOCAL BUTTON STYLES

const LocalBoldMediumButtonTextStyle =
TextStyle(color: localAccentColor, fontSize: MediumTextSize, fontWeight: FontWeight.w600, fontFamily: DefaultFontName);

ButtonStyle LocalButtonStyle =
ButtonStyle(
    minimumSize: MaterialStateProperty.all<Size>(const Size(200.0, 50.0)),
    backgroundColor: MaterialStateProperty.all<Color>(localAccentColor));
// LOCAL BUTTON STYLES

const textFieldDecoration = InputDecoration(
  hintStyle: TextStyle(
    color: TextColor,
    fontSize: MediumTextSize,
    fontFamily: DefaultFontName,
  ),
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: primaryColor, width: 1.0),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: primaryColor, width: 3.0),
  ),
);

const localTextFieldDecoration = InputDecoration(
  hintStyle: TextStyle(
    color: TextColor,
    fontSize: MediumTextSize,
    fontFamily: DefaultFontName,
  ),
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: localAccentColor, width: 1.0),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: localAccentColor, width: 3.0),
  ),
);