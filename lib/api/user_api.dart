
import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/base_http_dio.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/standard_response.dart';
import 'package:comidapp/models/user_response.dart';

class UserAPI {
  final BaseHttpDio baseHttpDio;
  final AuthenticationClient authenticationClient;

  UserAPI(this.baseHttpDio, this.authenticationClient);

  Future<HttpResponse<UserResponse>> getUserData(int userID) async {
    final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest('/getUserById/'+userID.toString(),
        method: 'GET',
        headers: {
          "x-access-token":token
        },
        parser: (data){
          return UserResponse.fromJson(data);
        }
    );
  }

  Future<HttpResponse<StandardResponse>> editProfileData(
      int userId,
      String name,
      String lastname,
      String aboutMe,
      String photo,
      String direction,
      String telephone
      ) async {
    //final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest<StandardResponse>('/updateUser/' + userId.toString(),
        method: 'PUT',
        data: {
          "name": name,
          "last_name": lastname,
          "about_me": aboutMe,
          "photo": photo,
          "direction": direction,
          "telephone": telephone
        },
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        },
        parser: (data){
          return StandardResponse.fromJson(data);
        }
    );
  }

}