
import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/base_http_dio.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/create_food_response.dart';
import 'package:comidapp/models/food_response.dart';
import 'package:comidapp/models/foods_response.dart';
import 'package:comidapp/models/standard_response.dart';

class FoodAPI{
 final BaseHttpDio baseHttpDio;
 final AuthenticationClient authenticationClient;

  FoodAPI(this.baseHttpDio, this.authenticationClient);



Future<HttpResponse<FoodsResponse>> getFoodByUser() async {
  final token = await authenticationClient.accessToken;
  return baseHttpDio.resquest('/myFoods',
    method: 'GET',
    headers: {
    "x-access-token":token
    },
    parser: (data){
    return FoodsResponse.fromJson(data);
    }
  );
}

 Future<HttpResponse<FoodsResponse>> getFoodsByUserId(int userId) async {
   final token = await authenticationClient.accessToken;
   return baseHttpDio.resquest('/foodByUser/'+userId.toString(),
       method: 'GET',
       headers: {
         "x-access-token":token
       },
       parser: (data){
         return FoodsResponse.fromJson(data);
       }
   );
 }

 Future<HttpResponse<FoodResponse>> getFoodByID(int foodID) async {
   final token = await authenticationClient.accessToken;
   return baseHttpDio.resquest('/getFoodDetail/'+foodID.toString(),
       method: 'GET',
       headers: {
         "x-access-token":token
       },
       parser: (data){
         return FoodResponse.fromJson(data);
       }
   );
 }

 Future<HttpResponse<FoodsResponse>> getFoodsbyCityName(String cityName) async {
   final token = await authenticationClient.accessToken;
   return baseHttpDio.resquest('/foodsByCity/'+cityName,
       method: 'GET',
       headers: {
         "x-access-token":token
       },
       parser: (data){
         return FoodsResponse.fromJson(data);
       }
   );
 }

Future<HttpResponse<CreateFoodResponse>> createFood(
    String name,
    String description,
    String pvp,
    String longitude,
    String latitude,
    String city,
    String maxTourist,
    String urlImage
    ) async {
  final token = await authenticationClient.accessToken;
  return baseHttpDio.resquest<CreateFoodResponse>('/food',
  method: 'POST',
      data: {
        "name": name,
        "description": description,
        "pvp": pvp,
        "longitude": longitude,
        "latitude": latitude,
        "city": city,
        "max_tourist": maxTourist,
        "image": urlImage
      },
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "x-access-token":token
    },
      parser: (data){
        return CreateFoodResponse.fromJson(data);
      }
  );
}

 Future<HttpResponse<StandardResponse>> updateFood(
     int foodId,
     String name,
     String description,
     String pvp,
     String longitude,
     String latitude,
     String city,
     String maxTourist,
     String urlImage
     ) async {
   final token = await authenticationClient.accessToken;
   return baseHttpDio.resquest<StandardResponse>('/updateFood/' + foodId.toString(),
       method: 'PUT',
       data: {
         "name": name,
         "description": description,
         "pvp": pvp,
         "longitude": longitude,
         "latitude": latitude,
         "city": city,
         "max_tourist": maxTourist,
         "image": urlImage
       },
       headers: {
         "Content-Type": "application/json; charset=utf-8",
         "x-access-token":token
       },
       parser: (data){
         return StandardResponse.fromJson(data);
       }
   );
 }


  }