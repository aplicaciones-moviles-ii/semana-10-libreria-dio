
class StandardResponse {

  final bool status;
  final String message;

  StandardResponse(
      this.status,
      this.message,);

  static StandardResponse fromJson(Map<String, dynamic> json){
    return StandardResponse(
        json['status'],
        json['message'],
    );
  }
}