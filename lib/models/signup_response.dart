
class SignUpResponse {
  final bool status;
  final String message;

  SignUpResponse(
      this.status,
      this.message);

  static SignUpResponse fromJson(Map<String, dynamic> json){
    return SignUpResponse(
      json['status'],
      json['message'],
    );
  }
}