
class SetReservationResponse {

  final bool status;
  final String message;

  SetReservationResponse(
      this.status,
      this.message);

  static SetReservationResponse fromJson(Map<String, dynamic> json){
    return SetReservationResponse(
        json['status'],
        json['message']
    );
  }
}