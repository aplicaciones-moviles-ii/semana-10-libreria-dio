// To parse this JSON data, do
//
//     final localFood = localFoodFromJson(jsonString);

import 'dart:convert';

import 'package:comidapp/models/food.dart';

FoodsResponse localFoodFromJson(String str) => FoodsResponse.fromJson(json.decode(str));

String localFoodToJson(FoodsResponse data) => json.encode(data.toJson());

class FoodsResponse {
  final bool status;
  final List<Food> foods;

  FoodsResponse({
    required this.status,
    required this.foods,
  });



  factory FoodsResponse.fromJson(Map<String, dynamic> json) => FoodsResponse(
    status: json["status"],
    foods: List<Food>.from(json["foods"].map((x) => Food.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "foods": List<dynamic>.from(foods.map((x) => x.toJson())),
  };
}
