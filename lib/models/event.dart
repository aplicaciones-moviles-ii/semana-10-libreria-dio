
class Event {
  Event({
    required this.id,
    required this.estado,
    required this.date,
    required this.hour,
    required this.createdAt,
    required this.updatedAt,
    required this.foodId,
  });

  final int id;
  final bool estado;
  final DateTime date;
  final String hour;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int foodId;

  factory Event.fromJson(Map<String, dynamic> json) => Event(
    id: json["id"],
    estado: json["estado"],
    date: DateTime.parse(json["date"]),
    hour: json["hour"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    foodId: json["foodId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "estado": estado,
    "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
    "hour": hour,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "foodId": foodId,
  };
}
