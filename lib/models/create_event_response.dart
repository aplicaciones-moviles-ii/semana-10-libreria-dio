
class CreateEventResponse {
  final bool status;
  final String message;

  CreateEventResponse(
      this.status,
      this.message);

  static CreateEventResponse fromJson(Map<String, dynamic> json){
    return CreateEventResponse(
        json['status'],
        json['message'],
    );
  }
}