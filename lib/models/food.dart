class Food {
  Food(
      this.id,
      this.name,
      this.description,
      this.pvp,
      this.latitude,
      this.longitude,
      this.likes,
      this.active,
      this.city,
      this.maxTourist,
      this.createdAt,
      this.updatedAt,
      this.userId,
      this.image
      );

  final int id;
  final String name;
  final String description;
  final String pvp;
  final String latitude;
  final String longitude;
  final int likes;
  final bool active;
  final String city;
  final int maxTourist;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int userId;
  final String image;

  static Food fromJson(Map<String, dynamic> json) {
    return Food(
        json["id"],
        json["name"],
        json["description"],
        json["pvp"],
        json["latitude"],
        json["longitude"],
        json["likes"],
        json["active"],
        json["city"],
        json["max_tourist"],
        DateTime.parse(json["createdAt"]),
        DateTime.parse(json["updatedAt"]),
        json["userId"],
        json["image"]
    );
  }


  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "description": description,
    "pvp": pvp,
    "latitude": latitude,
    "longitude": longitude,
    "likes": likes,
    "active": active,
    "city": city,
    "max_tourist": maxTourist,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "userId": userId,
    "image":image
  };
}
