import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/models/event.dart';
import 'package:flutter/material.dart';

class LocalEventItemComponent extends StatefulWidget {
  final int index, selectedIndex;
  final Event event;
  final Function onTap;

  const LocalEventItemComponent(
      {Key? key,
      required this.event,
      required this.onTap,
      required this.index,
      required this.selectedIndex})
      : super(key: key);

  @override
  State<LocalEventItemComponent> createState() =>
      _LocalEventItemComponentState();
}

class _LocalEventItemComponentState extends State<LocalEventItemComponent> {
  Color backgroundColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    final year = widget.event.date.year.toString();
    final month = widget.event.date.month.toString().padLeft(2, '0');
    final day = widget.event.date.day.toString().padLeft(2, '0');

    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 50,
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Text("Date:"),
            Text(
              "$year-$month-$day",
              style: RegularMediumTextStyle,
            ),
            const Text("Hour:"),
            Text(
              widget.event.hour.toString(),
              style: RegularMediumTextStyle,
            ),
          ],
        ),
      ),
    );
  }
}
