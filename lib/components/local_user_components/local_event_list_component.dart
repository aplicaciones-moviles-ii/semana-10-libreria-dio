import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/components/local_user_components/local_event_item_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/event.dart';
import 'package:comidapp/models/events_response.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class LocalEventListComponent extends StatefulWidget {
  final int idFood, selectedIndex;
  final Function onTap;

  const LocalEventListComponent(
      {Key? key,
      required this.idFood,
      required this.selectedIndex,
      required this.onTap})
      : super(key: key);

  @override
  State<LocalEventListComponent> createState() =>
      _LocalEventListComponentState();
}

class _LocalEventListComponentState extends State<LocalEventListComponent> {
  final EventAPI eventAPI = GetIt.instance<EventAPI>();

  Future<List<Event>> getEvents() async {
    HttpResponse<EventsResponse> response =
        await eventAPI.getEvents(widget.idFood);
    return response.data!.events;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Event>>(
        future: getEvents(),
        builder: (context, snapshot) {
          Widget child = const Text("Connecting");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          if (snapshot.data!.isNotEmpty) {
            child = ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  List<Event>? lista = snapshot.data;
                  return LocalEventItemComponent(
                      event: lista![index],
                      onTap: widget.onTap,
                      index: index,
                      selectedIndex: widget.selectedIndex);
                });
          } else {
            child = const Center(
                child: Text(
                  "NO EVENTS ADDED",
                  style: RegularLargeTextStyle,
                ));
          }
          return Container(
            child: child,
          );
        },
      ),
    );
  }
}
