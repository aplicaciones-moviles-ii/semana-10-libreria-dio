import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/components/general_components/circle_image_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/food.dart';
import 'package:comidapp/models/food_response.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class PartialFoodDataComponent extends StatelessWidget {
  final int foodId;
  final FoodAPI foodAPI = GetIt.instance<FoodAPI>();

  PartialFoodDataComponent(
      {Key? key,
      required this.foodId})
      : super(key: key);

  Future<FoodResponse> getFood() async {
    HttpResponse<FoodResponse> response =
        await foodAPI.getFoodByID(foodId);
    return response.data!;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<FoodResponse>(
      future: getFood(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
        Food? food = snapshot.data!.food;
        return Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            CircleImageComponent(urlImage: food.image),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  food.name,
                  style: BoldMediumTextStyle,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width*0.7,
                  child: Text(
                    food.description,
                    style: BoldSmallTextStyle,
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
