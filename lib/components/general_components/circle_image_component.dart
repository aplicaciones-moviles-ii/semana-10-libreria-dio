import 'package:flutter/material.dart';

class CircleImageComponent extends StatelessWidget {
  final urlImage;
  const CircleImageComponent({Key? key, this.urlImage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
          width: 50.0,
          height: 50.0,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      urlImage)
              )
          )),
    );
  }
}
