import 'package:comidapp/api/user_api.dart';
import 'package:comidapp/components/general_components/partial_user_data_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/food.dart';
import 'package:comidapp/models/user.dart';
import 'package:comidapp/models/user_response.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'food_image_component.dart';

class FoodItemComponent extends StatefulWidget {
  final Food food;
  final Function onTap;

  const FoodItemComponent({Key? key, required this.food, required this.onTap})
      : super(key: key);

  @override
  State<FoodItemComponent> createState() => _FoodItemComponentState();
}

class _FoodItemComponentState extends State<FoodItemComponent> {
  final UserAPI userAPI = GetIt.instance<UserAPI>();

  Future<User> searchUser() async {
    HttpResponse<UserResponse> response =
    await userAPI.getUserData(widget.food.userId);
    return response.data!.user;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap(widget.food);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          PartialUserDataComponent(userId:widget.food.userId),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: PaddingBetweenWidgets),
            child: Text(widget.food.name, style: BoldMediumTextStyle,),
          ),
          FoodImageComponent(price: widget.food.pvp, image: widget.food.image,),
          Padding(
            padding: const EdgeInsets.all(PaddingBetweenWidgets),
            child: Text(
              widget.food.description, style: RegularMediumTextStyle,),
          ),
        ],
      ),
    );
  }
}
